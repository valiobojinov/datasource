package com.estafet.datasource.node;

public interface IDataSourceNode {
	String getId();
	String getNodeName();
	String getNodeType();
}
