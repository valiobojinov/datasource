package com.estafet.datasource.statement;

import com.estafet.datasource.NodeNotCreatedException;
import com.estafet.datasource.NodeNotFoundException;
import com.estafet.datasource.node.IDataSourceNode;
import com.estafet.datasource.query.QueryCriteria;

public interface IQueryStatement<E extends IDataSourceNode, C extends QueryCriteria> extends IStatement {
	E query(C criteria) throws NodeNotFoundException, NodeNotCreatedException;

}
