package com.estafet.datasource;

import com.estafet.datasource.IDataSource;
import com.estafet.datasource.initializer.IDataSourceInitializer;

public abstract class AbstractDataSource<I extends IDataSourceInitializer, N> implements IDataSource<N> {

	private final I initializer;

	public AbstractDataSource(I initializer) {

		this.initializer = initializer;
	}

	protected I getDataSourceInitializer() {
		return initializer;
	}
}
